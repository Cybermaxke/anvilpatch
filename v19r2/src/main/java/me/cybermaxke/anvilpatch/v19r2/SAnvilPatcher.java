/*
 * This file is part of AnvilPatch, licensed under the MIT License (MIT).
 *
 * Copyright (c) Cybermaxke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package me.cybermaxke.anvilpatch.v19r2;

import static me.cybermaxke.anvilpatch.util.AnvilTextHelper.filterInput;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map.Entry;

import com.google.common.base.Throwables;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;

import me.cybermaxke.anvilpatch.AnvilPatcher;
import me.cybermaxke.anvilpatch.BiFunction;
import me.cybermaxke.anvilpatch.util.ReflectionHelper;

import net.minecraft.server.v1_9_R2.ItemStack;
import net.minecraft.server.v1_9_R2.PacketDataSerializer;
import net.minecraft.server.v1_9_R2.PacketPlayOutSetSlot;
import net.minecraft.server.v1_9_R2.ContainerAnvil;
import net.minecraft.server.v1_9_R2.EntityPlayer;
import net.minecraft.server.v1_9_R2.NetworkManager;
import net.minecraft.server.v1_9_R2.PacketPlayInCustomPayload;

import org.apache.commons.lang.StringUtils;

import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class SAnvilPatcher implements AnvilPatcher, Listener {

    private BiFunction<Player, String, String> textFormatter;
    private Plugin plugin;
    private Field fieldChannel;

    @Override
    public void patchServer(Plugin plugin, BiFunction<Player, String, String> textFormatter) {
        this.textFormatter = textFormatter;
        this.plugin = plugin;
        for (Player player : Bukkit.getOnlinePlayers()) {
            inject(player);
        }
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        inject(event.getPlayer());
    }

    /**
     * Gets the session of the entity player.
     *
     * @param entityPlayer The player
     * @return The channel
     */
    private Channel getChannel(EntityPlayer entityPlayer) throws Exception {
        NetworkManager nm = entityPlayer.playerConnection.networkManager;

        if (this.fieldChannel == null) {
            this.fieldChannel = ReflectionHelper.findField(NetworkManager.class, Channel.class, 0);
        }

        this.fieldChannel.setAccessible(true);
        return (Channel) this.fieldChannel.get(nm);
    }

    /**
     * Injects the message handler into the player.
     *
     * @param player The player
     */
    private void inject(Player player) {
        try {
            inject0(player);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    private void inject0(Player player) throws Exception {
        final EntityPlayer playerHandle = ((CraftPlayer) player).getHandle();

        final Channel channel = getChannel(playerHandle);
        final ChannelPipeline pipe = channel.pipeline();

        // Try to find the handler name
        final String handler = findHandler(pipe);

        if (handler == null) {
            throw new IllegalStateException("Unable to find the minecraft packet handler");
        }

        if (pipe.get("anvilpatch") == null) {
            pipe.addBefore(handler, "anvilpatch", new Handler(playerHandle));
        }
    }

    private String findHandler(ChannelPipeline pipe) {
        // This should normally be good enough
        if (pipe.get("packet_handler") != null) {
            return "packet_handler";
        }
        // Try to find the network manager under a different name
        for (Entry<String, ChannelHandler> en : pipe.toMap().entrySet()) {
            if (en.getValue() instanceof NetworkManager) {
                return en.getKey();
            }
        }
        // Not found :(
        return null;
    }

    private static final Method readStringMethod;

    static {
        Method method = null;
        try {
            method = PacketDataSerializer.class.getMethod("c", int.class);
            if (method.getReturnType().equals(String.class)) {
                method = null;
            }
        } catch (NoSuchMethodException ignored) {
        } catch (SecurityException e) {
            throw Throwables.propagate(e);
        }
        if (method == null) {
            try {
                method = PacketDataSerializer.class.getMethod("e", int.class);
            } catch (Exception e) {
                throw Throwables.propagate(e);
            }
        }
        readStringMethod = method;
    }

    private final class Handler extends ChannelDuplexHandler {

        private final EntityPlayer playerHandle;
        private final Thread mainThread;

        Handler(EntityPlayer playerHandle) {
            this.playerHandle = playerHandle;
            // The patch is applied in the main thread
            this.mainThread = Thread.currentThread();
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            if (msg instanceof PacketPlayInCustomPayload) {
                final PacketPlayInCustomPayload msg0 = (PacketPlayInCustomPayload) msg;
                if (msg0.a().equals("MC|ItemName")) {
                    if (Thread.currentThread() == this.mainThread) {
                        this.handleItemRename(msg0);
                    } else {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                handleItemRename(msg0);
                            }
                        }.runTask(plugin);
                    }
                    return;
                }
            }
            super.channelRead(ctx, msg);
        }

        void handleItemRename(PacketPlayInCustomPayload msg) {
            if (!(this.playerHandle.activeContainer instanceof ContainerAnvil)) {
                plugin.getLogger().warning("Received a unexpected item rename message, "
                        + "there is no anvil container opened. (On the server)");
                return;
            }
            final ContainerAnvil container = (ContainerAnvil) this.playerHandle.activeContainer;

            if (msg.b() != null && msg.b().readableBytes() >= 1) {
                String value;
                try {
                    value = (String) readStringMethod.invoke(msg.b(), Short.MAX_VALUE);
                } catch (Exception e) {
                    throw Throwables.propagate(e);
                }
                value = filterInput(value);
                value = textFormatter.apply(this.playerHandle.getBukkitEntity(), value);
                if (value.length() <= 30) {
                    container.a(value);
                    final ItemStack itemStack0 = container.getSlot(0).getItem();
                    final ItemStack itemStack1 = container.getSlot(1).getItem();
                    ItemStack itemStack2 = container.getSlot(2).getItem();
                    if (itemStack0 != null) {
                        if (itemStack2 == null) {
                            itemStack2 = itemStack0.cloneItemStack();
                            container.getSlot(2).set(itemStack2);
                        }
                        if (StringUtils.isEmpty(value)) {
                            if (itemStack0.hasName()) {
                                itemStack2.r();
                            }
                        } else if (!value.equals(itemStack0.getName())) {
                            itemStack2.c(value);
                        } else if (itemStack1 == null || itemStack1.getItem() == null) {
                            final PacketPlayOutSetSlot packet = new PacketPlayOutSetSlot(container.windowId, 2, null);
                            this.playerHandle.playerConnection.sendPacket(packet);
                            container.getSlot(2).set(null);
                        }
                    }
                }
            } else {
                container.a("");
            }
        }
    }
}
